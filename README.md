-Construccion de imagen-
docker build -t test-docker .

-Correr contenedor-
docker run -p 5000:5000 test-docker
docker run -p 80:5000 -e FLASK_ENV=development test-docker 

-correr contenedor-
docker-compose up
docker-compose up --build

-bajar contenedor-
docker-compose -down

-construir imagen desde docker compose-
docker-compose up --build

-ver contenedor-
docker-compose logs

-detener contenedores-
FOR /f "tokens=*" %i IN ('docker ps -q') DO docker stop %i

-eliminar contenedores detenidos-
docker container prune

-elimina todo-
docker system prune

-eliminar todas las imagenes o contenedores-
FOR /f "tokens=*" %i IN ('docker ps -aq') DO docker rm %i
FOR /f "tokens=*" %i IN ('docker images --format "{{.ID}}"') DO docker rmi %i  

-eliminar imagenes o contenedores por ID-
docker image rm
docker container rm

-ejecutar comando dentro de contenedor-
docker container exec [ID]
docker container exec -it [ID] sh

-manejo de bd-
docker container exec [ID] flask db init 
docker container exec [ID] flask db migrate 
docker container exec [ID] flask db upgrade
docker container exec [ID] flask db downgrade

-correr test-
docker container exec [ID] flask test

-python dentro del container-
docker container exec -it [ID] sh

-Creacion y activacion de entorno-
py -m venv env
.\venv\Scripts\activate
