# Rutas de acceso de la aplicación
from project import create_app
from flask import render_template

app = create_app()

@app.route('/')
def publications():

    template_name = 'publications.html'
    return render_template(template_name)

