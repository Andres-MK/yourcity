# Lugar donde se inicia la aplicación
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow

# Se hace llamado de SQLAchemy para inicializar la base de datos
db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()

#Crear objetos de configuracion

def register_blueprints(app):
    from project.posts.endpoints import posts_blueprint

    app.register_blueprint(posts_blueprint)

def create_app():
    app = Flask(__name__)

    # Se integran configuraciones a la app flask
    app_config = os.getenv('APP_CONFIG')
    app.config.from_object(app_config)

    # Se inicializa la base de datos con el parametro app de flask 
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)

    register_blueprints(app)
    return app