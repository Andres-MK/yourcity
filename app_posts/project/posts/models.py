from project import db
from app_client.project.users.models import User



class Posts(db.Model):
    id = db.Column(db.Integer, primary_key = True, autoincrement=True)
    metter = db.Column(db.String)
    body = db.Column(db.String)
    date_post = db.Column(db.String)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
