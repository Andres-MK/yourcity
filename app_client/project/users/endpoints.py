# Planos y puntos de entrada para conectar con flask
# variable = Blueprint('nombre', nombre modulo actual)
from flask import Blueprint, request, jsonify
from project import db
from project.users.models import User
from project.users.serializers import user_schema

users_blueprint = Blueprint('users', __name__)


def save_user(user):
    db.session.add(user)
    db.session.commit()


@users_blueprint.route('/users', methods=['POST'])
def create():

    user = user_schema.load(request.get_json())
    save_user(user)
    return user_schema.dump(user), 201


@users_blueprint.route('/users/<id>', methods=['PUT'])
def update(id):
    user = User.query.filter_by(id=id).first()

    if user is None:
        return 'Not found', 404

    user = user_schema.load(request.get_json(), instance=user)
    save_user(user)
    return user_schema.dump(user), 200


@users_blueprint.route('/users', methods=['GET'])
def list():
    users = User.query.all()

    return jsonify(user_schema.dump(users, many=True)), 200


@users_blueprint.route('/users/<id>', methods=['DELETE'])
def delete(id):
    user = User.query.filter_by(id=id).first()

    if user is None:
        return 'Not found', 404
    else:
        db.session.delete(user)
        db.session.commit()
        return "Eliminado exitosamente", 200
