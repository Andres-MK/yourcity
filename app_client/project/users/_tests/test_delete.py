from project import db
from project._tests.base import BaseTestCase
from project.users._tests.utils import create_user


class TestDelete(BaseTestCase):
    def test_delete(self):
        user = create_user()
        db.session.add(user)
        db.session.commit()

        with self.client:
            response = self.client.delete(
                f'/users/{user.id}',
            )

        self.assertStatus(response, 200)

    def test_delete_other_id(self):
        user = create_user()
        db.session.add(user)
        db.session.commit()

        with self.client:
            response = self.client.delete(
                f'/users/{2}',
            )

        self.assertStatus(response, 404)
