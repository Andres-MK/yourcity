import string
import random
from project.users.models import User


def random_string():
    return ''.join(
        [
            random.choice(string.ascii_letters)
            for _ in range(16)
        ])


def create_user():
    return User(
        name=random_string(),
        last_name=random_string(),
        civil_state=random_string()
    )
