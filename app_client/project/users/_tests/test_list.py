from project import db
from project._tests.base import BaseTestCase
from project.users._tests.utils import create_user


class TestList(BaseTestCase):
    def test_list(self):
        user1 = create_user()
        db.session.add(user1)
        db.session.commit()

        user2 = create_user()
        db.session.add(user2)
        db.session.commit()

        with self.client:
            response = self.client.get(
                '/users',
            )

        self.assertStatus(response, 200)
