from project import db
from project._tests.base import BaseTestCase
from project.users._tests.utils import create_user


class TestUpdate(BaseTestCase):
    def test_update(self):
        user = create_user()
        db.session.add(user)
        db.session.commit()

        data = {
            "name": "Actualizado",
            "last_name": "hola",
            "civil_state": "12356"
        }

        with self.client:
            response = self.client.put(
                f'/users/{user.id}',
                json=data
            )
        self.assertStatus(response, 200)

    def test_update_unexisting_user(self):
        data = {
            "name": "Actualizado",
            "last_name": "hola",
            "civil_state": "casado"
        }
        with self.client:
            response = self.client.put(
                '/users/1',
                json=data
            )
        self.assertStatus(response, 404)
