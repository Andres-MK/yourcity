from project._tests.base import BaseTestCase


class TestCreate(BaseTestCase):
    def _call_create(self, data):
        with self.client:
            return self.client.post(
                '/users',
                json=data
            )

    def test_create(self):
        data = {
            "name": "andres",
            "last_name": "hola",
            "civil_state": "soltero"
        }
        response = self._call_create(data)
        self.assertStatus(response, 201)

    def test_create_without_name(self):
        data = {
            "last_name": "hola",
            "civil_state": "soltero"
        }
        response = self._call_create(data)
        self.assertStatus(response, 400)

    def test_create_without_last_name(self):
        data = {
            "name": "andres",
            "civil_state": "soltero"
        }
        response = self._call_create(data)
        self.assertStatus(response, 400)

    def test_create_without_civil_state(self):
        data = {
            "name": "andres",
            "last_name": "hola"
        }
        response = self._call_create(data)
        self.assertStatus(response, 400)
